<h1 align="center"> composer require keli-saga/easyswoole_library </h1>

<p align="center"> saga easyswoole common library.</p>


## Installing

```shell
$ composer require keli-saga/easyswoole_library
```

## Usage

TODO

## Contributing

You can contribute in one of three ways:

1. File bug reports using the [issue tracker](https://github.com/keli/easyswoole_library/issues).
2. Answer questions or fix bugs on the [issue tracker](https://github.com/keli/easyswoole_library/issues).
3. Contribute new features or update the wiki.

_The code contribution process is not very formal. You just need to make sure that you follow the PSR-0, PSR-1, and PSR-2 coding guidelines. Any new code contributions must be accompanied by unit tests where applicable._

## License

MIT

### 我试试看好不好用