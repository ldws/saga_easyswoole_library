<?php
return [
    "LOG" => [
        // 设置记录日志文件时日志文件存放目录
        'dir' => LOG_DIR,
        // 设置记录日志时的日志最低等级，低于此等级的日志不进行记录和显示
        'level' => \EasySwoole\Log\LoggerInterface::LOG_LEVEL_DEBUG,
        // 设置日志处理器 `handler` (handler)
        'handler' => new \Keli\EasyswooleLibrary\Common\Log\LogHandler(LOG_DIR),
        // 设置开启在记录日志到日志文件时在控制台打印日志
        'logConsole' => true,
        // 设置开启在控制台显示日志
        'displayConsole' => true,
        // 设置打印日志时忽略哪些分类的日志不进行记录
        'ignoreCategory' => []
    ],
    'TEMP_DIR' => null,
    'MYSQL' => [
        'host' => '',
        'port' => 3306,
        'user' => '',
        'password' => '',
        'database' => '',
        'timeout' => 5,
        'charset' => 'utf8mb4',
    ],
    // 测试环境使用db-16
    'REDIS' => [
        'host' => '',
        'auth' => '',
        'port' => '6379',
        'serialize' => \EasySwoole\Redis\Config\RedisConfig::SERIALIZE_NONE,
        'db' => 16
    ],
    // 测试环境使用db-16
    'PREDIS' => [
        'scheme' => 'tcp',
        'host' => '',
        'port' => 6379,
        'password' => '',
        'db' => 16,
    ],
    'EASY_WECHAT' => [
        'open_platform' => [
            'app_id' => '',
            'secret' => '',
            // 指定 API 调用返回结果的类型：array(default)/collection/object/raw/自定义类名
            'response_type' => 'array',
            'http' => [
                'verify' => false,
            ],
        ]
    ],
];