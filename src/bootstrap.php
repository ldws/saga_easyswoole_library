<?php
//全局bootstrap事件
use GuzzleHttp\DefaultHandler;
use Yurun\Util\Swoole\Guzzle\SwooleHandler;

date_default_timezone_set('Asia/Shanghai');

//加载公共方法
require_once EASYSWOOLE_ROOT . "/App/Common/common.php";
//日志记录目录
const LOG_DIR = EASYSWOOLE_ROOT . "/Log/";
// guzzle非阻塞
DefaultHandler::setDefaultHandler(SwooleHandler::class);