<?php
/**
 * Created by PhpStorm.
 * User: dylan
 * Date: 2021/5/20
 * Time: 7:21 下午
 */

namespace Keli\EasyswooleLibrary\Common\Session;


use EasySwoole\Component\Singleton;

class Session extends \EasySwoole\Session\Session
{
    use Singleton;
}