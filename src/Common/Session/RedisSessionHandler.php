<?php
/**
 * Created by PhpStorm.
 * User: dylan
 * Date: 2021/2/25
 * Time: 1:29 下午
 */

namespace Keli\EasyswooleLibrary\Common\Session;


use EasySwoole\RedisPool\RedisPool;
use EasySwoole\Session\SessionHandlerInterface;

class RedisSessionHandler implements SessionHandlerInterface
{

    private $prefix = 'session_';//前缀
    private $redisPoolName = 'redis';//redispool名称
    private $ttl = 30 * 3600;//半小时过期

    public function __construct($prefix = 'session_', $redisPoolName = 'redis', $ttl = 30 * 3600)
    {
        $this->prefix = $prefix;
        $this->redisPoolName = $redisPoolName;
        $this->ttl = $ttl;
    }

    public function destroy($sessionId)
    {
        return RedisPool::invoke("", function (\EasySwoole\Redis\Redis $redis) use ($sessionId) {
            $redis->del($this->prefix . $sessionId);
        });
    }

    /**
     * @param string $prefix
     */
    public function setPrefix(string $prefix): void
    {
        $this->prefix = $prefix;
    }

    /**
     * @param string $redisPoolName
     */
    public function setRedisPoolName(string $redisPoolName): void
    {
        $this->redisPoolName = $redisPoolName;
    }

    /**
     * @param float|int $ttl
     */
    public function setTtl($ttl): void
    {
        $this->ttl = $ttl;
    }

    function open(string $sessionId, ?float $timeout = null): bool
    {
        // TODO: Implement open() method.
    }

    function read(string $sessionId, ?float $timeout = null): ?array
    {
        return RedisPool::invoke($this->redisPoolName, function (\EasySwoole\Redis\Redis $redis) use ($sessionId) {
            $redis->expire($this->prefix . $sessionId, $this->ttl);
            return $redis->get($this->prefix . $sessionId);
        });
    }

    function write(string $sessionId, array $data, ?float $timeout = null): bool
    {
        return RedisPool::invoke($this->redisPoolName, function (\EasySwoole\Redis\Redis $redis) use ($sessionId, $data) {
            return $redis->set($this->prefix . $sessionId, $data, $this->ttl);
        });
    }

    function close(string $sessionId, ?float $timeout = null): bool
    {
        // TODO: Implement close() method.
    }

    function gc(int $expire, ?float $timeout = null): bool
    {
        // TODO: Implement gc() method.
    }
}