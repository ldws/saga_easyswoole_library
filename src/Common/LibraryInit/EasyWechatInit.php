<?php
/**
 * Created by PhpStorm.
 * User: dylan
 * Date: 2021/5/23
 * Time: 10:21 上午
 */

namespace Keli\EasyswooleLibrary\Common\LibraryInit;


use EasySwoole\Component\Di;
use EasySwoole\EasySwoole\Config;
use EasyWeChat\Factory;
use Symfony\Component\Cache\Adapter\RedisAdapter;

class EasyWechatInit
{

    public static function init()
    {
        $config = Config::getInstance()->getConf("EASY_WECHAT.open_platform");
        $predisConfig = Config::getInstance()->getConf("PREDIS");
        if (empty($config)) {
            throw new \Exception("EASY_WECHAT.open_platform is empty, 请检查配置");
        }
        if (empty($predisConfig)) {
            throw new \Exception("PREDIS is empty, 请检查配置");
        }
        //初始化easywechat sdk
        $openPlatform = Factory::openPlatform($config);

        $client = new \Predis\Client($predisConfig);
        // 创建缓存实例
        $cache = new RedisAdapter($client);
        // 替换应用中的缓存
        $openPlatform->rebind('cache', $cache);
        // 注入容器
        Di::getInstance()->set("openPlatform", $openPlatform);
    }
}