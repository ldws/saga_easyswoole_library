<?php
/**
 * Created by PhpStorm.
 * User: dylan
 * Date: 2023/8/6
 * Time: 22:40
 */

namespace Keli\EasyswooleLibrary\Common\RocketMQ;

use EasySwoole\EasySwoole\Config;
use EasySwoole\EasySwoole\Logger;
use MQ\Exception\AckMessageException;
use MQ\Exception\MessageNotExistException;
use MQ\MQClient;

class Consumer
{
    private $client;
    private $consumer;
    private $handler;

    public function __construct($configKey, $handler)
    {
        $config = Config::getInstance()->getConf($configKey);
        if (empty($config)) {
            Logger::getInstance()->error("RocketMQ config is empty");
            throw new \Exception("RocketMQ config is empty");
        }
        $this->client = new MQClient(
        // 设置HTTP协议客户端接入点，进入云消息队列 RocketMQ 版控制台实例详情页面的接入点区域查看。
            $config['endpoint'],
            // AccessKey ID，阿里云身份验证标识。获取方式，请参见创建AccessKey。
            $config['accessId'],
            // AccessKey Secret，阿里云身份验证密钥。获取方式，请参见创建AccessKey。
            $config['accessKey']
        );
        // 消息所属的Topic，在云消息队列 RocketMQ 版控制台创建。
        $topic = $config["topic"];
        // Topic所属的实例ID，在云消息队列 RocketMQ 版控制台创建。
        // 若实例有命名空间，则实例ID必须传入；若实例无命名空间，则实例ID传入null空值或字符串空值。实例的命名空间可以在云消息队列 RocketMQ 版控制台的实例详情页面查看。
        $instanceId = $config["instanceId"];
        $groupId = $config["groupId"];
        $this->consumer = $this->client->getConsumer($instanceId, $topic, $groupId);
        $this->handler = $handler;
    }

    public function ackMessages($receiptHandles)
    {
        try {
            $this->consumer->ackMessage($receiptHandles);
        } catch (\Exception $e) {
            if ($e instanceof AckMessageException) {
                // 某些消息的句柄可能超时，会导致消费确认失败。
                Logger::getInstance()->error(Sprintf("Ack Error, RequestId:%s\n", $e->getRequestId()));
                foreach ($e->getAckMessageErrorItems() as $errorItem) {
                    Logger::getInstance()->error(Sprintf("\tReceiptHandle:%s, ErrorCode:%s, ErrorMsg:%s\n", $errorItem->getReceiptHandle(), $errorItem->getErrorCode(), $errorItem->getErrorCode()));
                }
            }
        }
    }

    public function run()
    {
        // 在当前线程循环消费消息，建议多开个几个线程并发消费消息。
        while (True) {
            try {
                // 长轮询消费消息。
                // 若Topic内没有消息，请求会在服务端挂起一段时间（长轮询时间），期间如果有消息可以消费则立即返回客户端。
                $messages = $this->consumer->consumeMessage(
                    16, // 一次最多消费3条（最多可设置为16条）。
                    3 // 长轮询时间3秒（最多可设置为30秒）。
                );
            } catch (\MQ\Exception\MessageResolveException $e) {
                // 当出现消息Body存在不合法字符，无法解析的时候，会抛出此异常。
                // 可以正常解析的消息列表。
                $messages = $e->getPartialResult()->getMessages();
                // 无法正常解析的消息列表。
                $failMessages = $e->getPartialResult()->getFailResolveMessages();
                $receiptHandles = array();
                foreach ($messages as $message) {
                    // 处理业务逻辑。
                    $receiptHandles[] = $message->getReceiptHandle();
                    Logger::getInstance()->info(Sprintf("Consume %s: %s\n", $message->getMessageId(), $message->getMessageBodyMD5()));
                }
                foreach ($failMessages as $failMessage) {
                    // 处理存在不合法字符，无法解析的消息。
                    $receiptHandles[] = $failMessage->getReceiptHandle();
                    Logger::getInstance()->info(Sprintf("FailMsgID:%s, Reason:%s\n", $failMessage->getMessageId(), $failMessage->getErrorMessage()));
                }
                $this->ackMessages($receiptHandles);
                continue;
            } catch (\Exception $e) {
                if ($e instanceof MessageNotExistException) {
                    // 没有消息可以消费，继续轮询。
                    Logger::getInstance()->info(Sprintf("No message, contine long polling!RequestId:%s\n", $e->getRequestId()));
                    continue;
                }
                sleep(3);
                continue;
            }
            Logger::getInstance()->info("consume finish, messages:\n");
            // 处理业务逻辑。
            $receiptHandles = array();
            foreach ($messages as $message) {
                // 转到业务处理方法
                $result = $this->handler->process($message);
//                处理成功的消息写入待确认队列
                if ($result) {
                    $receiptHandles[] = $message->getReceiptHandle();
                }
            }
            // $message->getNextConsumeTime()前若不确认消息消费成功，则消息会被重复消费。
            // 消息句柄有时间戳，同一条消息每次消费拿到的都不一样。
            // ack messages $receiptHandles
            $this->ackMessages($receiptHandles);
            Logger::getInstance()->info("consume success, messages:" . json_encode($messages));
        }

    }
}