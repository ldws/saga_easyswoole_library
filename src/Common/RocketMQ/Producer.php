<?php

namespace Keli\EasyswooleLibrary\Common\RocketMQ;

use EasySwoole\EasySwoole\Config;
use EasySwoole\EasySwoole\Logger;
use MQ\Model\TopicMessage;
use MQ\MQClient;

class Producer
{
    private $client;
    private $producer;

    public function __construct($configKey)
    {
        $config = Config::getInstance()->getConf($configKey);
        if (empty($config)) {
            Logger::getInstance()->error("RocketMQ config is empty");
            throw new \Exception("RocketMQ config is empty");
        }
        $this->client = new MQClient(
        // 设置HTTP协议客户端接入点，进入云消息队列 RocketMQ 版控制台实例详情页面的接入点区域查看。
            $config['endpoint'],
            // AccessKey ID，阿里云身份验证标识。获取方式，请参见创建AccessKey。
            $config['accessId'],
            // AccessKey Secret，阿里云身份验证密钥。获取方式，请参见创建AccessKey。
            $config['accessKey']
        );
        // 消息所属的Topic，在云消息队列 RocketMQ 版控制台创建。
        $topic = $config['topic'];
        // Topic所属的实例ID，在云消息队列 RocketMQ 版控制台创建。
        // 若实例有命名空间，则实例ID必须传入；若实例无命名空间，则实例ID传入null空值或字符串空值。实例的命名空间可以在云消息队列 RocketMQ 版控制台的实例详情页面查看。
        $instanceId = $config['instanceId'];
        $this->producer = $this->client->getProducer($instanceId, $topic);
    }

    public function produce($message, $retry = 3, $messageKey = "", $properties = [])
    {
        $publishMessage = new TopicMessage(
            json_encode($message)
        );
        if (count($properties) > 0) {
            foreach ($properties as $key => $value) {
                $publishMessage->putProperty($key, $value);
            }
        }
        if ($messageKey != "") {
            $publishMessage->setMessageKey($messageKey);
        }
        $result = $this->producer->publishMessage($publishMessage);
        Logger::getInstance()->info("Send mq message success. msgId is:" . $result->getMessageId() . ", bodyMD5 is:" . $result->getMessageBodyMD5() . "\n");
        if ($result != NULL) {
            return $result->getMessageId();
        } else {
            logger::getInstance()->error("Send mq message failed. msgId is:" . $result->getMessageId() . ", bodyMD5 is:" . $result->getMessageBodyMD5() . "\n");
            // 重试3次
            if ($retry > 0) {
                $this->produce($message, $retry - 1, $messageKey, $properties);
            }
            return false;
        }
    }

    public function generateEventId($eventName, $mchId)
    {
        return sprintf("%s_%d_%s", $eventName, $mchId, uniqid());
    }
}