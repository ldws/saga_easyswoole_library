<?php
/**
 * Created by PhpStorm.
 * User: dylan
 * Date: 2021/2/25
 * Time: 1:13 下午
 */

namespace Keli\EasyswooleLibrary\Common\Base\ControllerBase;


abstract class ApiBase extends Base
{
    public function onRequest(?string $action): ?bool
    {
        if (!parent::onRequest($action)) {
            return false;
        }
        return true;
    }
}