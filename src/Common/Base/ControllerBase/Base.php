<?php
/**
 * Created by PhpStorm.
 * User: dylan
 * Date: 2021/5/20
 * Time: 6:06 下午
 */

namespace Keli\EasyswooleLibrary\Common\Base\ControllerBase;


use EasySwoole\Component\Context\ContextManager;
use EasySwoole\EasySwoole\Logger;
use EasySwoole\Http\AbstractInterface\Controller;
use EasySwoole\Http\Message\Status;
use EasySwoole\Utility\SnowFlake;
use EasySwoole\Validate\Validate;
use Keli\EasyswooleLibrary\Common\Context\ContextHandler;

class Base extends Controller
{
    // 将post及网关参数封装起来的数组
    public $requestDataArray;
    // 校验
    public $validator;
    // 网关authData
    public $authData;
    // 网关authHeader
    public $authHeader;
    // mch_id
    public $mch_id;
    // sub_mch_id
    public $sub_mch_id;
    // app_id
    public $app_id;

    public function onRequest(?string $action): ?bool
    {
        if (!parent::onRequest($action)) {
            return false;
        }
        // 初始化server
        try {
            if (!$this->init()) {
                return false;
            }
        } catch (\Exception $exception) {
            // 发生异常直接抛异常
            throw new \Exception($exception);
        }
        return true;
    }

    //基础参数初始化，校验所有接口必传参数
    public function init()
    {
        try {
            // 请求参数初始化
            $this->paramsInit();

            // 网关初始化及内部请求初始化
            $this->requestInit();

            // 组件初始化，用于初始化公共组件
            $this->componentInit();

            // 初始化上下文，将变量写入上下文环境
            $this->ContextInit();

        } catch (\Throwable $exception) {
            throw new \Exception($exception);
        }
        // AOP 请求日志
        Logger::getInstance()->info("requestData:" . json_encode($this->requestDataArray));
        return true;
    }

    // 请求参数初始化
    public function paramsInit()
    {
        //获取POST请求数据
        $postData = $this->request()->getBody()->__toString();
        $postDataArray = [];
        if ($postData) {
            // 必须是json，统一走 json post
            $postDataArray = json_decode($postData, true);
            if (!is_array($postDataArray)) {
                $postDataArray = [];
            }
        }
        $this->requestDataArray = $postDataArray;
    }

    // 组件初始化
    public function componentInit()
    {
        $this->validator = new Validate();
    }

    // 网关请求初始化
    public function requestInit()
    {
        //header获取mch_id信息，由网关层上游传递
        $this->authHeader = $this->request()->getHeader('x-auth-data');
        //  若存在authHeader，证明是从网关过来的，则判断authHeader合法性，mch_id/authData等信息以网关侧为准
        //  若不存在authHeader，证明是内网inner请求，则不判断authHeader合法性，mch_id需手工传入，否则不通过
        if ($this->authHeader && isset($this->authHeader[0]) && $this->authHeader[0] != "") {
            // 网关请求参数校验
            $authData = json_decode($this->authHeader[0], true);
            if (!is_array($authData) || !isset($authData['mch_id'])) {
                // 抛出异常，日志由上层异常捕获并记录
                throw new \Exception("requestInit: authData 不合法 authHeader" . print_r($this->authHeader, true) . "requestDataArray" . print_r($this->requestDataArray, true), Status::CODE_BAD_REQUEST);
            }
            //初始化网关变量，将传递的敏感参数使用网关参数覆盖
            $this->gatewayRequestParamsInit($authData);
        } else {
            // inner请求参数校验
            if (!isset($this->requestDataArray['mch_id']) || $this->requestDataArray['mch_id'] == "" || !is_numeric($this->requestDataArray['mch_id'])) {
                throw new \Exception("requestInit: mch_id 不能为空" . print_r($this->authHeader, true) . "requestDataArray" . print_r($this->requestDataArray, true), Status::CODE_BAD_REQUEST);
            }
            // 初始化内部请求参数变量
            $this->innerParamsInit($this->requestDataArray);
        }
    }

    public function innerParamsInit($requestDataArray = [])
    {
        if (isset($requestDataArray["mch_id"])) {
            $this->mch_id = $requestDataArray["mch_id"];
        }
        if (isset($requestDataArray["sub_mch_id"])) {
            $this->sub_mch_id = $requestDataArray["sub_mch_id"];
        }
        if (isset($requestDataArray["app_id"])) {
            $this->app_id = $requestDataArray["app_id"];
        }
    }

    //网关参数初始化
    public function gatewayRequestParamsInit($auth = [])
    {
        // 聚合业务参数
        if (isset($auth["auth_data"])) {
            $authData = $auth["auth_data"];
            // 微信openid
            if (isset($authData['openid'])) {
                $this->requestDataArray['openid'] = $authData['openid'];
            }
            // 微信session_key
            if (isset($authData['session_key'])) {
                $this->requestDataArray['session_key'] = $authData['session_key'];
            }
            // 租户app_id
            if (isset($authData['app_id'])) {
                $this->requestDataArray['app_id'] = $authData['app_id'];
                $this->app_id = $authData['app_id'];
            }
            // 会员id
            if (isset($authData['member_user_id'])) {
                $this->requestDataArray['member_user_id'] = $authData['member_user_id'];
            }
            // 微信appid
            if (isset($authData['wechat_app_id'])) {
                $this->requestDataArray['wechat_app_id'] = $authData['wechat_app_id'];
            }
        }
        // 租户id
        if (isset($auth['mch_id'])) {
            $this->requestDataArray['mch_id'] = $auth['mch_id'];
            $this->mch_id = $auth['mch_id'];
        }
        // 将authData全部放入requestDataArray，方便上层使用
        if (isset($auth['auth_data'])) {
            $this->requestDataArray['auth_data'] = $auth;
            $this->authData = $auth;
        }
    }

    // 上下文初始化，同worker下多协程
    public function ContextInit()
    {
        // 下游traceId 若traceId为空，则自己生成
        if ($this->request()->getHeader('trace-id') != "") {
            ContextManager::getInstance()->set(ContextHandler::TRACE_ID, $this->request()->getHeader('trace-id')[0]);
        } else {
            ContextManager::getInstance()->set(ContextHandler::TRACE_ID, SnowFlake::make(1, 1));
        }
        ContextManager::getInstance()->set(ContextHandler::MCH_ID, $this->mch_id);
        ContextManager::getInstance()->set(ContextHandler::SUB_MCH_ID, $this->sub_mch_id);
    }

    /**
     * json返回
     * @param int $statusCode
     * @param null $result
     * @param null $msg
     * @return bool
     * @throws \Exception
     */
    protected function writeJson($statusCode = 200, $result = null, $msg = null)
    {
        if (!$this->response()->isEndResponse()) {
            $data = array(
                "code" => $statusCode,
                "result" => $result,
                "msg" => $msg,
                "trace_id" => ContextManager::getInstance()->get(ContextHandler::TRACE_ID),
            );

            // AOP 响应日志
            Logger::getInstance()->info("responseData:" . print_r(json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES), true));

            $this->response()->write(json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
            $this->response()->withHeader('Content-type', 'application/json;charset=utf-8');
            $this->response()->withStatus($statusCode);
            return true;
        } else {
            return false;
        }
    }

    // 控制器异常处理
    protected function onException(\Throwable $throwable): void
    {
        $statusCode = $throwable->getCode() ? $throwable->getCode() : 500;
        // 控制器统一异常返回，并向上抛出交给全局异常处理类
        $this->writeJson($statusCode, null, $throwable->getMessage());
        // 记录一个error级别的log，方便排查问题
        Logger::getInstance()->Error($throwable->getMessage());
        // 继续向上抛出，记录未捕获的异常，收拢优化排查
        throw new \Exception($throwable);
    }

    protected function actionNotFound(?string $action): void
    {
        $this->writeJson(Status::CODE_NOT_FOUND, $action);
    }

    public function index()
    {
        // TODO: Implement index() method.
        $this->actionNotFound('index');
    }
}