<?php
/**
 * Created by PhpStorm.
 * User: dylan
 * Date: 2021/2/25
 * Time: 2:15 下午
 */

namespace Keli\EasyswooleLibrary\Common\Base\ModelBase;

use EasySwoole\ORM\AbstractModel;
use EasySwoole\ORM\Db\Connection;
use EasySwoole\ORM\DbManager;

class ModelBase extends AbstractModel
{
    // 是否分库
    public $multiTable = false;
    // 分表模式 $multiTable为true时生效 1、以租户ID(mch_id)为后缀分表（租户隔离表） 2、以租户ID(mch_id)后1位分表（10表）3、以租户ID(mch_id)后2位分表（100表）
    public $multiTableMode = 1;

    // 是否分表
    public $multiDB = false;
    // 分库模式 $multiDB为true时生效 1、以租户ID(mch_id)为后缀分库（租户隔离库） 2、以租户ID(mch_id)后1位分库（10库）3、以租户ID(mch_id)后2位分库（100库）
    public $multiDBMode = 1;

    public function __construct($mch_id = 0, array $data = [])
    {
        parent::__construct($data);

        // 分表逻辑
        if ($this->multiTable) {
            // 以租户ID(mch_id)为后缀分表（租户隔离表）
            switch ($this->multiTableMode) {
                case 1:
                    $this->tableName($this->tableName . "_" . $mch_id);
                    break;
                case 2:
                    // 以租户ID(mch_id)后1位分库（10库）
                    $this->tableName($this->tableName . "_" . substr($mch_id, -1));
                    break;
                case 3:
                    // 以租户ID(mch_id)后2位分库（100库）
                    $this->tableName($this->tableName . "_" . substr($mch_id, -2));
                default:
                    throw new \Exception("ModelBase: 分表模式multiTableMode参数不正确，请检查。mch_id:{$mch_id} multiTable:{$this->multiTable} multiTableMode:{$this->multiTableMode}");
            }
            $this->tableName($this->tableName . "_" . $mch_id);
        }
        // 分库逻辑
        if ($this->multiDB) {
            // 以租户ID(mch_id)为后缀分库（租户隔离库）
            switch ($this->multiDBMode) {
                case 1:
                    $dbName = $this->dbName . "_" . $mch_id;
                    $connectionName = "connection_" . $dbName;
                    break;
                case 2:
                    $dbName = $this->dbName . "_" . substr($mch_id, -1);
                    $connectionName = "connection_" . $dbName;
                    break;
                case 3:
                    $dbName = $this->dbName . "_" . substr($mch_id, -2);
                    $connectionName = "connection_" . $dbName;
                    break;
                default:
                    throw new \Exception("ModelBase: 分库模式multiDBMode参数不正确，请检查。mch_id:{$mch_id} multiDB:{$this->multiDB} multiDBMode:{$this->multiDBMode}");
            }
            // 获取连接
            $client = DbManager::getInstance()->getConnection($connectionName);
            if (!$client) {
                // 若连接不存在，则创建连接
                try {
                    $config = \EasySwoole\EasySwoole\Config::getInstance()->getConf("MYSQL");
                    $mysqlConfig = new \EasySwoole\ORM\Db\Config($config);
                    $mysqlConfig->setDatabase($dbName);
                    $dbManager = DbManager::getInstance()->addConnection(new Connection($mysqlConfig), "connection_" . $this->dbName);
                    $client = $dbManager->getConnection($connectionName);
                    if ($client) {
                        $this->connectionName = $connectionName;
                    } else {
                        throw new \Exception("分库初始化失败，未找到数据库" . $dbName);
                    }
                } catch (\Exception $exception) {
                    throw new \Exception($exception->getMessage());
                }
            }
            $this->connectionName = $connectionName;
        }
    }
}