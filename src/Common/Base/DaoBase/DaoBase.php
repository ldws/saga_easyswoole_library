<?php
/**
 * Created by PhpStorm.
 * User: dylan
 * Date: 2021/2/25
 * Time: 2:16 下午
 */

namespace Keli\EasyswooleLibrary\Common\Base\DaoBase;

class DaoBase
{
    public $mch_id;
    public $sub_mch_id;

    public function __construct($mch_id = 0, $sub_mch_id = 0)
    {
        $this->mch_id = $mch_id;
        $this->sub_mch_id = $sub_mch_id;
    }
}