<?php
/**
 * Created by PhpStorm.
 * User: dylan
 * Date: 2021/5/20
 * Time: 2:19 下午
 */

namespace Keli\EasyswooleLibrary\Common\Exception;

use EasySwoole\EasySwoole\Trigger;
use EasySwoole\Http\Request;
use EasySwoole\Http\Response;

class ExceptionHandler
{
    public static function handle(\Throwable $exception, Request $request, Response $response)
    {
        // 统一日志记录
//        Logger::getInstance()::log($exception->getMessage(), LoggerInterface::LOG_LEVEL_ERROR);
//        Logger::getInstance()::log($exception->getTraceAsString(), LoggerInterface::LOG_LEVEL_ERROR);
        Trigger::getInstance()->error($exception->getMessage());
    }
}