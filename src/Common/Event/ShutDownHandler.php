<?php
/**
 * Created by PhpStorm.
 * User: dylan
 * Date: 2021/5/23
 * Time: 9:54 上午
 */

namespace Keli\EasyswooleLibrary\Common\Event;


class ShutDownHandler
{
    public static function handle()
    {
        // 开发者对 shutdown 进行处理
    }
}