<?php
/**
 * Created by PhpStorm.
 * User: dylan
 * Date: 2021/5/23
 * Time: 9:46 上午
 */

namespace Keli\EasyswooleLibrary\Common\Event;


use EasySwoole\EasySwoole\Logger;

class AfterRequestHandler
{
    public static function handle(\EasySwoole\Http\Request $request, \EasySwoole\Http\Response $response)
    {
        // AOP 请求日志
        Logger::getInstance()->info("responseBody:" . json_encode($response->getBody()));
    }
}