<?php
/**
 * Created by PhpStorm.
 * User: dylan
 * Date: 2021/5/23
 * Time: 9:46 上午
 */

namespace Keli\EasyswooleLibrary\Common\Event;


use EasySwoole\EasySwoole\Logger;

class OnRequestHandler
{
    public static function handle(\EasySwoole\Http\Request $request, \EasySwoole\Http\Response $response)
    {
        //获取POST请求数据
        $postData = $request->getBody()->__toString();
        $postDataArray = [];
        if ($postData) {
            $postDataArray = json_decode($postData, true);
            if (!is_array($postDataArray)) {
                $postDataArray = [];
            }
        }
        $queryData = $request->getQueryParams();
        $requestDataArray = array_merge($queryData, $postDataArray);
        // AOP 请求日志
        Logger::getInstance()->info("requestBody:" . json_encode($requestDataArray));
    }
}