<?php
/**
 * Created by PhpStorm.
 * User: dylan
 * Date: 2021/5/23
 * Time: 9:58 上午
 */

namespace Keli\EasyswooleLibrary\Common\ResourceInit;


use EasySwoole\EasySwoole\Config;
use EasySwoole\Redis\Config\RedisConfig;

class RedisPoolInit
{
    public static function init()
    {
        $redisConfig = new RedisConfig(Config::getInstance()->getConf('REDIS'));
        $redisPoolConfig = \EasySwoole\RedisPool\RedisPool::getInstance()->register($redisConfig);
        $redisPoolConfig->setMinObjectNum(5);
        $redisPoolConfig->setMaxObjectNum(20);
    }
}