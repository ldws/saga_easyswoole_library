<?php
/**
 * Created by PhpStorm.
 * User: dylan
 * Date: 2021/5/23
 * Time: 9:59 上午
 */

namespace Keli\EasyswooleLibrary\Common\ResourceInit;


use EasySwoole\EasySwoole\Config;
use EasySwoole\ORM\Db\Connection;
use EasySwoole\ORM\DbManager;

class MysqlPoolInit
{
    public static function init()
    {
        $config = Config::getInstance()->getConf("MYSQL");
        $mysqlConfig = new \EasySwoole\ORM\Db\Config($config);
        //连接池配置
        $mysqlConfig->setGetObjectTimeout(3.0); //设置获取连接池对象超时时间
        $mysqlConfig->setIntervalCheckTime(30 * 1000); //设置检测连接存活执行回收和创建的周期
        $mysqlConfig->setMaxIdleTime(15); //连接池对象最大闲置时间(秒)
        $mysqlConfig->setMinObjectNum(5); //设置最小连接池存在连接对象数量
        $mysqlConfig->setMaxObjectNum(20); //设置最大连接池存在连接对象数量
        $mysqlConfig->setAutoPing(5); //设置自动ping客户端链接的间隔
        //默认connection_name为default
        DbManager::getInstance()->addConnection(new Connection($mysqlConfig));
    }
}