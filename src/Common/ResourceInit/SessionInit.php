<?php
/**
 * Created by PhpStorm.
 * User: dylan
 * Date: 2021/5/23
 * Time: 9:57 上午
 */

namespace Keli\EasyswooleLibrary\Common\ResourceInit;


use Keli\EasyswooleLibrary\Common\Session\RedisSessionHandler;
use Keli\EasyswooleLibrary\Common\Session\Session;

class SessionInit
{
    public static function init()
    {
        $handler = new RedisSessionHandler();
        //表示cookie name   还有save path
        Session::getInstance($handler);
    }
}