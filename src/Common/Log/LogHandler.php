<?php
/**
 * Created by PhpStorm.
 * User: dylan
 * Date: 2021/2/25
 * Time: 1:24 下午
 */

namespace Keli\EasyswooleLibrary\Common\Log;


use EasySwoole\Component\Context\ContextManager;
use EasySwoole\Log\LoggerInterface;
use Keli\EasyswooleLibrary\Common\Context\ContextHandler;
use Keli\EasyswooleLibrary\Common\Lib\WebLib;

class LogHandler implements LoggerInterface
{

    private $logDir;

    function __construct(string $logDir = null)
    {
        if (empty($logDir)) {
            $logDir = getcwd();
        }
        $this->logDir = $logDir;
    }

    function log(?string $msg, int $logLevel = self::LOG_LEVEL_INFO, string $category = 'debug'): string
    {
        $debugTrace = debug_backtrace();
        // 因为log被包了一层，所以需要去两个头
        array_shift($debugTrace);
        array_shift($debugTrace);
//        array_shift($debugTrace);
        $logCaller = array_shift($debugTrace);
        $caller = array_shift($debugTrace);
        //精确到毫秒的时间戳，日期格式
        $time = microtime(true);
        $micro = sprintf("%06d", round(1000000 * ($time - intval($time))));
        $time = intval($time);
        $date = date('Y-m-d H:i:s', $time);

        //日志等级
        $levelStr = $this->levelMap($logLevel);

        $msgArr = [
            "level" => $levelStr,
            "server_name" => \EasySwoole\EasySwoole\Config::getInstance()->getConf("SERVER_NAME"),
            "datetime" => $date . $micro,
            // 调用文件路径，即调用log方法的文件路径
            "caller_file" => $logCaller['file'] ?? "",
            // 调用代码行
            "caller_line" => $logCaller['line'] ?? "",
            // 类名
            "caller_class" => $caller['class'] ?? "",
            // 参数
            "caller_args" => $caller['args'] ?? "",
            // 方法
            "caller_function" => $caller['function'] ?? "",
            // 调用类型
            "caller_type" => $caller['type'] ?? "",
            // 日志内容
            "msg" => $msg,
            "pid" => getmypid(),
            "memory" => memory_get_usage(),
            "client_ip" => WebLib::getClientIp(),
            "server_ip" => WebLib::getServerIp(),
        ];

        // 上下文写入Log
        $msgArr[ContextHandler::MCH_ID] = ContextManager::getInstance()->get(ContextHandler::MCH_ID);
        $msgArr[ContextHandler::SUB_MCH_ID] = ContextManager::getInstance()->get(ContextHandler::SUB_MCH_ID);
        $msgArr[ContextHandler::TRACE_ID] = ContextManager::getInstance()->get(ContextHandler::TRACE_ID);

        //构建日志写入格式 时间、日志级别、日志(日志中包含trace)
        $str = json_encode($msgArr) . "\n";
        // 记录jsonLog
        $this->jsonLog($str, $category);
        return $str;
    }

    function console(?string $msg, int $logLevel = self::LOG_LEVEL_DEBUG, string $category = 'debug')
    {
        $date = date('Y-m-d H:i:s');
        $levelStr = $this->levelMap($logLevel);
        echo "[{$date}][{$category}][{$levelStr}]:[{$msg}]\n";
    }

    private function levelMap(int $level)
    {
        switch ($level) {
            case self::LOG_LEVEL_INFO:
                return 'info';
            case self::LOG_LEVEL_NOTICE:
                return 'notice';
            case self::LOG_LEVEL_WARNING:
                return 'warning';
            case self::LOG_LEVEL_ERROR:
                return 'error';
            default:
                return 'unknown';
        }
    }

    private function arrayLog($arrayMsg = [])
    {
        $jsonLogDir = $this->logDir . "/log";
        $logfileSuffix = date('Y-m-d');
        $jsonLogFile = $jsonLogDir . "/log_{$logfileSuffix}.log";
        if (!is_dir($jsonLogDir)) {
            mkdir($jsonLogDir, 0777, true);
        }
        $str = print_r($arrayMsg, true) . "\n";
        file_put_contents($jsonLogFile, $str, FILE_APPEND | LOCK_EX);
        return $str;
    }

    // 记录json日志
    private function jsonLog($str, $category = "debug")
    {
        $jsonLogDir = $this->logDir . "/jsonlog";
        $logfileSuffix = date('Y-m-d');
        $jsonLogFile = $jsonLogDir . "/{$category}_log_{$logfileSuffix}.log";
        if (!is_dir($jsonLogDir)) {
            mkdir($jsonLogDir, 0777, true);
        }
        file_put_contents($jsonLogFile, $str, FILE_APPEND | LOCK_EX);
        return $str;
    }
}