<?php
/**
 * Created by PhpStorm.
 * User: dylan
 * Date: 2021/2/25
 * Time: 1:17 下午
 */

namespace Keli\EasyswooleLibrary\Common\Context;


class ContextHandler
{
    const MCH_ID = "mch_id";
    const SUB_MCH_ID = "sub_mch_id";
    const TRACE_ID = "trace_id";
}