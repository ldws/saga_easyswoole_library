<?php
/**
 * Created by PhpStorm.
 * User: dylan
 * Date: 2021/5/23
 * Time: 9:38 上午
 */

namespace Keli\EasyswooleLibrary\Common\FrameWorkInit;


use EasySwoole\Component\Di;
use EasySwoole\EasySwoole\AbstractInterface\Event;
use EasySwoole\EasySwoole\Swoole\EventRegister;
use EasySwoole\EasySwoole\SysConst;
use Keli\EasyswooleLibrary\Common\Event\AfterRequestHandler;
use Keli\EasyswooleLibrary\Common\Event\OnRequestHandler;
use Keli\EasyswooleLibrary\Common\Event\ShutDownHandler;
use Keli\EasyswooleLibrary\Common\Exception\ExceptionHandler;
use Keli\EasyswooleLibrary\Common\LibraryInit\EasyWechatInit;
use Keli\EasyswooleLibrary\Common\ResourceInit\MysqlInit;
use Keli\EasyswooleLibrary\Common\ResourceInit\MysqlPoolInit;
use Keli\EasyswooleLibrary\Common\ResourceInit\RedisInit;
use Keli\EasyswooleLibrary\Common\ResourceInit\RedisPoolInit;
use Keli\EasyswooleLibrary\Common\ResourceInit\SessionInit;

class EasySwooleEventBase implements Event
{

    public static function initialize()
    {
        date_default_timezone_set('Asia/Shanghai');
        // 全局异常处理
        Di::getInstance()->set(SysConst::HTTP_EXCEPTION_HANDLER, [ExceptionHandler::class, 'handle']);
        // 全局OnRequest事件
        Di::getInstance()->set(SysConst::HTTP_GLOBAL_ON_REQUEST, [OnRequestHandler::class, 'handle']);
        // 全局AfterRequest事件
        Di::getInstance()->set(SysConst::HTTP_GLOBAL_AFTER_REQUEST, [AfterRequestHandler::class, 'handle']);
        // 开发者自定义设置 shutdown
        Di::getInstance()->set(SysConst::SHUTDOWN_FUNCTION, [ShutDownHandler::class, 'handle']);

        // MYSQL初始化
        MysqlPoolInit::init();
        // Redis初始化
        RedisPoolInit::init();
        // 初始化分布式session
        SessionInit::init();
        // easywechat init
        EasyWechatInit::init();
    }

    public static function mainServerCreate(EventRegister $register)
    {
    }
}