<?php
/**
 * Created by PhpStorm.
 * User: dylan
 * Date: 2021/1/9
 * Time: 11:58 上午
 */
if (!function_exists('genTraceLog')) {
// 传入trace
    function genTraceLog($trace, $msg)
    {
        //精确到毫秒的时间戳，日期格式
        $time = microtime(true);
        $micro = sprintf("%06d", round(1000000 * ($time - intval($time))));
        $time = intval($time);
        $date = date('Y-m-d H:i:s', $time);
        $logArr = [
            "server_name" => \EasySwoole\EasySwoole\Config::getInstance()->getConf("SERVER_NAME"),
            "datetime" => $date . $micro,
            "file" => $trace['file'],
            "class" => $trace['class'],
            "function" => $trace['function'],
            "line" => $trace['line'],
            "msg" => $msg,
            "pid" => getmypid(),
            "memory" => memory_get_usage(),
            "trace" => $trace['trace'],
            "client_ip" => \Keli\EasyswooleLibrary\Lib\WebLib::getClientIp(true),
            "server_ip" => \Keli\EasyswooleLibrary\Lib\WebLib::getServerIp(true),
        ];
        return json_encode($logArr);
    }
}

if (!function_exists('genLog')) {
    // 带上下文的日志
    function genLog($msg, $context)
    {
        // 上下文数据强校验，内部统一日志记录方式
        if (!isset($context["file"]) || !!isset($context["dir"]) || !isset($context["class"]) || isset($context["function"]) || !isset($context['method'])) {
            throw new \Exception("日志记录上下文传参有误，请检查, msg: {$msg}");
        }
        //精确到毫秒的时间戳，日期格式
        $time = microtime(true);
        $micro = sprintf("%06d", round(1000000 * ($time - intval($time))));
        $time = intval($time);
        $date = date('Y-m-d H:i:s', $time);
        $logArr = [
            "server_name" => \EasySwoole\EasySwoole\Config::getInstance()->getConf("SERVER_NAME"),
            "datetime" => $date . $micro,
            "dir" => $context['dir'],
            "file" => $context['file'],
            "class" => $context['class'],
            "function" => $context['function'],
            "method" => $context['method'],
            "line" => $context['line'],
            "msg" => $msg,
            "pid" => getmypid(),
            "memory" => memory_get_usage(),
            "client_ip" => \Keli\EasyswooleLibrary\Lib\WebLib::getClientIp(true),
            "server_ip" => \Keli\EasyswooleLibrary\Lib\WebLib::getServerIp(true),
        ];
        return json_encode($logArr);
    }
}
if (!function_exists('genLogPath')) {
    // 日志记录path
    function genLogPath($debug_backtrace)
    {
        $class = $debug_backtrace[0]['class'];
        return str_replace("\\", "-", $class);
    }
}